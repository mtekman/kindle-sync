;;; ksync.el --- Scripts to interact with the kindle-sync shell script -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Mehmet Tekman <mtekman89@gmail.com>

;; Author: Mehmet Tekman
;; URL: https://gitlab.com/mtekman/ksync.el
;; Keywords: outlines
;; Package-Requires: ((emacs "25.1"))
;; Version: 0.1

;;; Commentary:

;; TODO, fix this section


;;; Code:
(defgroup ksync nil
  "Kindle-Sync configuration group."
  :group 'emacs)

(defcustom ksync--envtable-name "Environment Variables"
  "String to match the table name which contains Environment variables."
  :type 'string
  :group 'ksync)

(defcustom ksync-setup-file "~/repos/_mtekman/kindle-sync/config/setup.org"
  "Path to the configuration file containing all tables.

  Inside, there should be a named table of Environment variables
  matching the ``ksync--envtable-name'' string, and this
  should list the kindle-sync repository location which we will
  use to determine the location of the shell script."
  :type 'file
  :group 'ksync)

(defvar ksync--shellscript nil
  "This is determined by ``ksync--get-shellscript'' if nil.")

(defun ksync--get-shellscript ()
  "Determines the location the ksync shell script by parsing the Environment Variables table."
  (unless ksync--shellscript
    (setq ksync--shellscript (expand-file-name "ksync" (alist-get "REPO_DIR" (ksync--get-envvars) nil nil #'string=))))
  ksync--shellscript)

(defun ksync--get-envvars ()
  "Store the environment variables from the ``ksync--envtable-name'' table into an alist."
  (let* ((env-table-pos (car (delq nil
                                   (mapcar (lambda (x) (if (string= ksync--envtable-name (car x)) (cdr x)))
                                           (ksync--get-tablenamepos)))))
         (tab-contents (with-current-buffer (find-file-noselect ksync-setup-file)
                         (save-excursion
                           (prog2 (goto-char env-table-pos)
                               (cdr (member 'hline (org-table-to-lisp))))))))
    (mapcar (lambda (x) (cons (substring-no-properties (car x))
                         (let ((porval (substring-no-properties (cadr x))))
                           (if (file-exists-p porval)
                               (expand-file-name porval)
                             porval))))
            tab-contents)))

(defun ksync--get-tablenamepos (&optional func)
  "Get all table names and content positions, and optionally run FUNC on the name and position."
  (let ((parsetree (org-element-parse-buffer)))
    (org-element-map parsetree 'table
      (lambda (x)
        (let ((name (org-element-property :name x))
              (cbeg (org-element-property :contents-begin x)))
          (if func
              (funcall func name cbeg)
            (cons name cbeg)))))))

(defun ksync--export--currenttable (table-name)
  "Export table at current point.  Assumes TABLE-NAME is known."
  (let* ((config_dir (alist-get "CONFIG_DIR" (ksync--get-envvars) nil nil #'string=))
         (table_path (expand-file-name (concat table-name ".csv") (expand-file-name "tables" config_dir))))
    (cond ((not config_dir)
           (user-error "CONFIG_DIR not found in the Environment table"))
          ((string-match ksync--envtable-name table-name)
           (user-error "Please do not export the Environment Variables table: '%s'" table-name))
          ((string-match "[ &]" table-name)
           (user-error "Please use a different table name without spaces: '%s'" table-name))
          (t (progn (org-table-export table_path "orgtbl-to-csv")
                    (message "Exported: %s" table_path))))))

(defun ksync--get-tablename-atpoint ()
  "Get name of the table at point."
  (plist-get (cadr (plist-get
                    (cadr (org-element-at-point))
                    :parent)) :name))

(defun ksync-export-table-atpoint (&optional no-yn)
  "Export table under the cursor and do a Y/N prompt, unless NO-YN is true."
  (interactive)
  (unless (org-at-table-p) (user-error "No table at point"))
  (org-table-align)
  (let ((table-name (ksync--get-tablename-atpoint)))
    (if (or no-yn (yes-or-no-p (format "Export '%s'? " table-name)))
        (ksync--export--currenttable table-name))))

(defun ksync-export-table (table-name)
  "Export table with TABLE-NAME."
  (interactive
   (list
    (completing-read
     "Export Table: "
     (delq nil
           (mapcar (lambda (x) (if (string= ksync--envtable-name (car x)) nil (car x)))
                   (ksync--get-tablenamepos)))
     nil t)))
  (let ((table-pos (alist-get table-name (ksync--get-tablenamepos) nil nil #'string=)))
    (with-current-buffer (find-file-noselect ksync-setup-file)
      (save-excursion
        (goto-char table-pos)
        (ksync--export--currenttable table-name)))))

(defun ksync-export-alltables ()
  "Export all tables in the buffer."
  (interactive)
  (let ((tnames nil))
    (ksync--get-tablenamepos
     (lambda (name _pos)
       (unless (string= ksync--envtable-name name)
         (ksync-export-table name)
         (push name tnames))))
    (message "Exported: %s\n      to: %s"
             (mapconcat #'identity tnames ", ")
             (expand-file-name "tables" (alist-get "CONFIG_DIR" (ksync--get-envvars)
                                                   nil nil #'string=)))))


(defun ksync-initialise (&optional client-name)
  "Initialise all clients, or for a single CLIENT-NAME."
  (interactive (list (completing-read "Initialise Client: " (append '("all") (ksync--get-clientnames)))))
  (let* ((client-name (or client-name "all"))
         (env-string (mapconcat (lambda (x) (concat (car x) "=" (cdr x))) (ksync--get-envvars) " "))
         (com-string (concat (ksync--get-shellscript) "initialise" client-name))
         (commandstr (concat env-string " " com-string)))
    (shell-command commandstr)
    (message "Ksync: initialised %s" client-name)))

(defun ksync-cronjob (operation &optional client-name)
  "Run OPERATION (update or clear) on all cronjobs,  or for a single CLIENT-NAME."
  (interactive (list (completing-read "Operation: " '("update" "clear"))
                     (completing-read "Client: " (append '("all") (ksync--get-clientnames)))))
  (let* ((client-name (or client-name "all"))
         (env-string (mapconcat (lambda (x) (concat (car x) "=" (cdr x))) (ksync--get-envvars) " "))
         (com-string (concat (ksync--get-shellscript) "cronjob" client-name operation))
         (commandstr (concat env-string " " com-string)))
    (shell-command commandstr)
    (message "Ksync: cronjob %s on %s" operation client-name)))

(defun ksync--get-clientnames ()
  "Get a list of all client names."
  (let* ((machine-pos (alist-get "CLIENTS" (ksync--get-tablenamepos) nil nil #'string=))
         (tab-contents (with-current-buffer (find-file-noselect ksync-setup-file)
                         (save-excursion
                           (prog2 (goto-char machine-pos)
                               (cdr (member 'hline (org-table-to-lisp))))))))
    (mapcar (lambda (x) (substring-no-properties (car x))) tab-contents)))

(defun ksync ()
  "On Ctrl-C Ctrl-C, export or and update the tables."
  (interactive)
  (let ((elem-info (car (org-element-at-point))))
    (if (and (or (equal elem-info 'table)
                 (equal elem-info 'table-row))
             (org-at-table-p))
        (let ((table-name (ksync--get-tablename-atpoint)))
          (cond ((string= table-name "CLIENTS")
                 (let ((mach-action (completing-read "Clients: " '("Initialise Clients" "Export Config"))))
                   (cond ((string= mach-action "Initialise Clients") (call-interactively #'ksync-initialise))
                         ((string= mach-action "Export Config") (ksync-export-table-atpoint t))
                         (t (user-error "No such action")))))
                ((string= table-name ksync--envtable-name)
                 (user-error "The %s table should not be exported" ksync--envtable-name))
                ;; Otherwise, export the specific table
                (t (ksync-export-table-atpoint))))
      ;; Otherwise export all tables
      (call-interactively #'ksync-export-table))
    (if (yes-or-no-p "Cron: Update all jobs?")
        (ksync-cronjob "update" "all"))))

;;(add-hook 'org-ctrl-c-ctrl-c-hook #'ksync)


(provide 'ksync)
;;; ksync.el ends here
