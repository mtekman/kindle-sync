#!/bin/bash
widthxheight="$1"
org_git="$2"
org_dir="$3"
projfile="$4"
opt="$5"
oname="$6"
tags="$7"

# - Env variables
SUB_SCRIPT_DIR=${SUB_SCRIPT_DIR:-~/repos/_mtekman/kindle-sync/server/media-scripts/submodules/}
KINDLE_CONFIG_DIR=${KINDLE_CONFIG_DIR:-~/.config/kindle-sync}
EMACS_DIR=${KINDLE_CONFIG_DIR}/emacs_config

mkdir -p ${EMACS_DIR} 2> /dev/null

if ! [[ "$opt" =~ (agenda|sparse) ]] || [ "$oname" == "" ]; then
    echo "
    $(basename $0) <WIDTHxHEIGHT> <github-repository> <github-directory> <project-file> (agenda|parse) <output-filename> [tag1,tag2]


" && exit 255
fi

org_dirname="$(dirname $org_dir)"
head_last_epoch=$(stat ${org_dir}/.git/FETCH_HEAD -c%X)
mins30_ago=$(date --date="30 minutes ago" +%s)

if ! [ -d "$org_dir" ]; then
    echo -e "\e[32m[Git] Cloning Repo\e[0m"
    mkdir -p $org_dirname
    cd $org_dirname
    git clone $org_git $(basename $org_dir)
    cd - > /dev/null
elif [ $mins30_ago -lt $head_last_epoch ]; then
    echo -e "\e[32m[Git] Last fetched less than 30 minutes ago. Skipping pull.\e[0m"
else
    echo -e "\e[32m[Git] Pulling latest\e[0m"
    cd $org_dir
    git pull origin master
    cd - > /dev/null
fi

# Always :work:+TODO
##emacs --batch -Q --load ./emacs-org-config.el --dir=fire --mode="${opt}" --filename="${oname}"
## Batch does not allow screenshots to happen (obviously)
emacs -Q --load ${SUB_SCRIPT_DIR}/emacs-script.el --dir="${EMACS_DIR}" --mode="${opt}" \
      --filename="${oname}" --wxh="${widthxheight}" \
      --tags="${tags}" --projfile="${projfile}" &&
    echo "Generated: $oname"
