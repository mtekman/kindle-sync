#!/bin/bash
oname="$1"
key="$2"
wdth=$(echo "$3" | sed -r 's|([0-9]+)x([0-9]+)-([0-9]+)|\1|')
hght=$(echo "$3" | sed -r 's|([0-9]+)x([0-9]+)-([0-9]+)|\2|')
bord=$(echo "$3" | sed -r 's|([0-9]+)x([0-9]+)-([0-9]+)|\3|')
img=$(echo "$4" | sed -r 's|(.*)::(.*)|\1|')
txt=$(echo "$4" | sed -r 's|(.*)::(.*)|\2|')

if [ "$key" == "" ]; then
    echo "
    $(basename $0) <output-image-name> <key> <width>x<height>-border [image-file::text]

Where KEY retrieves a previously generated image and copies to output, or if IMAGE-FILE does not exist and TEXT is given, then the newly generated content is generated under KEY, and then retrieved.

" && exit 255
fi

if [ -e $oname ]; then
    ## If a file exists and are providing items - we are overwriting
    ## Backup the existing with timestamps
    #if [ "$4" = "::" ]; then ## || [ "$4" != "" ]
        echo "Retrieving file: $oname"
        exit 0
    #else
    #    ext=$(echo $oname | sed -r 's|(.*)\.([a-z]+)|\2|g')
    #    prefix=$(echo $oname | sed -r 's|(.*)\.([a-z]+)|\1|g')
    #    backup="${prefix}."$(date +"%Y-%m-%d_%H-%M")
    #    ## Perform the move
    #    mv -v $oname $backup
    #fi
fi

# At this point we assume the file does not exist
if [ "$txt" = "" ] && [ "$img" = "" ] || [ "$txt" = "$img" ]; then
    echo "File does not exist, must give txt OR img" >&2
    exit 255
fi

file_txt=$(mktemp).png
file_img=$(mktemp).png


## In every case, prep first with a 1 pixel high nothing
convert -size ${wdth}x1 xc:white ${file_txt}

if [ "$txt" != "" ]; then
    ## Now generate txt of minimum height for each line

    ## We change the line delimiter to a likely unused char '}'
    readarray -td'}' txt_array <<< $(echo "${txt}\n" | sed 's|\\n|}|g');
    declare -p txt_array > /dev/null 2>&1

    num_elems=$(( ${#txt_array[@]} - 1 )) ## skip last element which is always blank
    
    for line in "${txt_array[@]::${num_elems}}"; do
        # backup last loops cumulative image
        last_png=$(mktemp).png
        new_png=$(mktemp).png
        cp ${file_txt} ${last_png}
        # append all text
        convert -background white -fill black \
                -size $(( ${wdth} - 2*${bord} ))x \
                -font Cantarell \
                label:"$line" \
                -bordercolor white -border ${bord}x$(( $bord/2 )) \
                -gravity south \
                ${new_png}

        convert -append ${last_png} ${new_png} ${file_txt}
    done
fi

# Calculate remaining space
txwidth=$(file ${file_txt} \
              | grep -oP "[0-9]+\s.\s[0-9]+" \
              | sed -r 's|[0-9]+\sx\s([0-9]+)|\1|')
remaining_vspace=$(( ${hght} - ${txwidth} ))

## Generate img of minimum height
if [ "$img" != "" ]; then
    ## Resize image to remaining space
    convert ${img} -resize ${wdth}x${remaining_vspace} \
            -size ${wdth}x${remaining_vspace} \
            xc:white +swap -gravity center -composite ${file_img}
else
    ## Or make blank from remaining space
    convert -size ${wdth}x${remaining_vspace} \
            xc:white ${file_img}
fi
## concat files
convert -append ${file_txt} ${file_img} ${oname}

# Final sanity check
if [ "$(file ${oname} | grep -oP '[0-9]+\s.\s[0-9]+')" != "${wdth} x ${hght}" ]; then
    echo "Final adjustments to fix image" >&2
    mogrify ${oname} -resize ${wdth}x${hght} -background white \
            -gravity center -extent ${wdth}x${hght}
fi

# Change to 8-bit, since eips can only produce that
tmpfile=$(mktemp).png
mv ${oname} ${tmpfile}
convert ${tmpfile} -define png:color-type=0 \
        -define png:bit-depth=8 \
        -define png:exclude-chunks=all \
        ${oname}

echo "Generated: $oname"
