
(defun parse-args (args)
  "Parse a list of ARGS which consists of '--<X>=<Y>' elements."
  (delete nil
	  (mapcar (lambda (it)
		    (if (and (string-prefix-p "--" it)
			     (string-match-p "=" it))
			(let* ((x (split-string it "="))
			       (y (cadr x))
			       (x (cadr (split-string (car x) "--"))))
			  (cons x y))))
		  args)))

;; Parse all Arguments
(setq pargs (parse-args command-line-args))

(setq projfile (alist-get "projfile" pargs nil nil 'string=))
(setq u-e-d (alist-get "dir" pargs nil nil 'string=))
(setq u-mode (alist-get "mode" pargs nil nil 'string=))
(setq outfile (alist-get "filename" pargs nil nil 'string=))
(setq usetags (split-string (alist-get "tags" pargs nil nil 'string=) ","))
(setq usetags (if (> (length (car usetags)) 0) usetags '("work")))  ;; default to "work"
(setq wandh (mapcar (lambda (x) (string-to-number x))
                    (split-string (alist-get "wxh" pargs nil nil 'string=) "x"))
      p-width (nth 0 wandh)
      p-height (nth 1 wandh))

(unless (or u-e-d u-mode)
  (user-error "Please specify user-emacs-directory as --dir=, and the operation as --mode="))

(setq user-emacs-directory u-e-d)

;; Main
(eval-when-compile
  (setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")
        ("org" . "https://orgmode.org/elpa/")))
  (package-initialize)
  (if (package-installed-p 'use-package)
      (require 'use-package)
    (package-refresh-contents)
    (package-install 'use-package)))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq inhibit-startup-screen t
      cursor-type 'nil)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(global-tab-line-mode -1)

(use-package org
  :ensure org-plus-contrib
  :init
  (setq org-hide-emphasis-markers t)
  ;;(require 'cl)
  :config
  (setq org-agenda-files (list projfile)
        org-refile-target-names projfile
        org-refile-targets '((nil :maxlevel . 5)
                             (org-refile-target-names :maxlevel . 2))
        org-log-done 'time
        org-src-window-setup 'current-window
        org-src-fontify-natively t
        org-src-tab-acts-natively t ;; indenting in tabs
        org-use-speed-commands t
        org-highlight-sparse-tree-matches nil
        org-use-fast-todo-selection 'expert
        org-todo-keywords
        '((sequence "TODO(t)" "DOING(d)" "PAUSED(p)" "WAITING(w)" "SOMEDAY(s)" "|" "DONE" "CANCELLED"))
        org-todo-keyword-faces
        '(("TODO" . (:foreground "DodgerBlue" :weight ultra-bold))
          ("DOING" . (:foreground "Red" :weight ultra-bold))
          ("PAUSED" . (:foreground "DarkSlateGray" :weight extra-bold))
          ("SOMEDAY" . (:foreground "FireBrick4" :weight extra-bold))
          ("WAITING" . (:foreground "LightBlue4" :weight extra-bold)))
        ;;org-agenda-hide-tags-regexp "@.*" ;; hide @contexts tags in the tags column
        atypical-format " %i %-12:T"
        org-refile-use-outline-path t ;; full refile paths
        org-outline-path-complete-in-steps nil ;; Show full path when autocompleting
        ))
(use-package org-superstar
  :after org
  :ensure t
  :config
  (setq org-superstar-headline-bullets-list '("◈" "※" "◎" "◌" "✢" "✽" "✿" "◈" "☀")
        org-superstar-leading-bullet " ")
  :hook (org-mode . org-superstar-mode))
(use-package org-super-agenda
  :ensure t
  :after org
  :config
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (org-super-agenda-mode 1)
  (setq org-super-agenda-groups
        '(;; Each group has an implicit boolean OR operator between its selectors.
          (:todo "TODO")
          (:todo "WAITING")
          ;;(:name "Habits" :habit t :order 1)
          ;;(:name "Work" :time-grid t :tag ("work") :order 2)
          ;;(:name "Hobbies" :time-grid t :tag ("life" "evening") :order 3)
          )))
(use-package org-ql
  :ensure t)

(defun screenshot (filename)
  (with-temp-file filename
    (insert (x-export-frames nil 'png))
    (message "Screenshoted: %s" filename)
    (kill-new filename)))

(with-current-buffer (find-file projfile)
  (setq cursor-type 'nil)
  (progn
    (setq frame-resize-pixelwise t)
    (set-frame-position (selected-frame) 0 0)
    (set-frame-size (selected-frame) p-width p-height t)
    (cond ((string= u-mode "sparse")
           (progn (org-ql-sparse-tree `(and (todo) (tags ,@usetags)))
                  (with-current-buffer (find-file projfile)
                    (goto-char 1)
                    (screenshot outfile))))
          ((string= u-mode "agenda")
           (progn (org-agenda nil "a")
                  (switch-to-buffer "*Org Agenda*")
                  (delete-other-windows)
                  (screenshot outfile)))
          (t (user-error "No such option: %s" u-mode)))
    (save-buffers-kill-emacs 1)))
;;))
