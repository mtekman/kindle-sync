const puppeteer = require("puppeteer");
const fs = require("fs");
const PNG = require("pngjs").PNG;

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

let map = {
    "week"  : {"url" : "https://calendar.google.com/calendar/u/0/r/week",
               "zoom": 0.9},
    "month" : {"url" : "https://calendar.google.com/calendar/u/0/r/month",
               "zoom": 0.9},
    "agenda": {"url" : "https://calendar.google.com/calendar/u/0/r/agenda",
               "zoom": 1},
    "4day"  : {"url" : "https://calendar.google.com/calendar/u/0/r/customday",
               "zoom": 1}
}

var dims = (process.argv[2] || "600x800").split("x").map(x => parseInt(x))
const width = dims[0];
const height= dims[1];

const wkey = process.argv[3] || "agenda";
const outf = process.argv[4] || "dash.png";

const choice = map[wkey];
const URL = choice["url"];
const scaleFactor = choice["zoom"];

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
	    executablePath: '/usr/bin/chromium',
	    args: ["--user-data-dir=~/.config/chromium/"],
    });
    const page = await browser.newPage();

    // await page.evaluateOnNewDocument(() => {
    //     Object.defineProperty(window, 'navigator', {
    //         value: new Proxy(navigator, {
    //             has: (target, key) => (key === 'webdriver' ? false : key in target),
    //             get: (target, key) =>
    //             key === 'webdriver'
    //                 ? undefined
    //                 : typeof target[key] === 'function'
    //                 ? target[key].bind(target)
    //                 : target[key]
    //         })
    //     })  
    // })   

    await page.setViewport({
        width: parseInt(width / scaleFactor),
        height: parseInt(height / scaleFactor),
        deviceScaleFactor: scaleFactor,
    });
    /* Might want to use networkidle0 here, depending on the type of page */
    /* See https://github.com/puppeteer/puppeteer/blob/main/docs/api.md */
    await page.goto(URL, { waitUntil: "networkidle2" });
    await sleep(3000);
    await page.screenshot({ path: outf });
    await fs.createReadStream(outf)
        .pipe(new PNG({ colorType: 0 }))
        .on("parsed", function () {
            this.pack().pipe(fs.createWriteStream(outf));
        });
    browser.close();
})();
