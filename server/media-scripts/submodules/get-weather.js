const puppeteer = require("puppeteer");

const fs = require("fs");
const PNG = require("pngjs").PNG;

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

let weather = {
    "openwmap" : {
        "url" : "https://openweathermap.org/city/",
        "scf" : 0.80, "scrollTo" : ".grid-container grid-4-5",
    }
}

var dims = (process.argv[2] || "600x800").split("x").map(x => parseInt(x))
const width = dims[0];
const height= dims[1];

const wkey = process.argv[3] || "openwmap";
const city = process.argv[4] || "XXXXXXX";  // replace this with city number
const outf = process.argv[5] || "dash.png";
//
const URL = weather[wkey]["url"] + weather[wkey]["city"];
const scaleF = weather[wkey]["scf"];
const isID = weather[wkey]["scrollTo"][0] === ".";
const cname = weather[wkey]["scrollTo"].substring(1);

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
	    executablePath: '/usr/bin/chromium',
	    //args: ["--user-data-dir=/home/tetris/.config/chromium/"],
    });
    const page = await browser.newPage();

    await page.setViewport({
        width: parseInt(width / scaleF),
        height: parseInt(height / scaleF),
        deviceScaleFactor: scaleF,
    });

    await page.goto(URL, { waitUntil: "networkidle2" });

    await sleep(1000);
    await page.evaluate((cname) => {
        document.getElementById("nav-website").remove()
        document.getElementById("stick-footer-panel").remove()
        document.getElementsByClassName(cname)[0].scrollIntoView();
        return 0;
    }, cname)
    await sleep(1000);
    await page.screenshot({ path: outf });
    await fs.createReadStream(outf)
        .pipe(new PNG({ colorType: 0 }))
        .on("parsed", function () {
            this.pack().pipe(fs.createWriteStream(outf));
        });
    browser.close();
})();
