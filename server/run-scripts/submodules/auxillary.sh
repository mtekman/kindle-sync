#!/bin/bash

CONFIG_DIR=${CONFIG_DIR:-/tmp/}
SERVER_LOG_FILE=${SERVER_LOG_FILE:-~/kindle-log.txt}
DATE_FMT="+%Y-%m-%d|%H:%M:%S"

date_cmd() {
    date "$DATE_FMT" "$@"
}

log_this() {
    local client=${CLIENT_NAME:-$1}
    echo "$(date_cmd) | $client | ${@:2}" | tee -a $SERVER_LOG_FILE
}

list_clients(){
    local machines=${CONFIG_DIR}/tables/CLIENTS.csv
    cut -d, -f 1 $machines | grep -v "Name"
}

get_file(){
    if ! [ -e $1 ]; then
        echo "Cannot find $1" >&2
        if [ "$2" != "" ]; then
            echo "Possible Options are: "$(ls $2)  >&2
        fi
        exit 255
    fi
    echo $1
}

get_userip() {
    local machines=${CONFIG_DIR}/tables/CLIENTS.csv
    if ! [ -e $machines ]; then
        echo "Cannot find the CLIENTS.csv file at $CONFIG_DIR"
        exit 255
    fi
    local client="$1"
    local colname="User@IP"
    local colnum=$(head -1 $machines | grep -oP ".*${colname}" | grep -o , | wc -l)
    colnum=$(( $colnum + 1 ))
    if [ "$client" = "all" ]; then
        local userip=$(cat $machines | cut -d, -f $colnum | grep -v "${colname}")
    else
        local userip=$(grep "^${client}," $machines | cut -d, -f $colnum | grep -v "${colname}")
    fi
    if [ "$userip" = "" ]; then
        echo -e "Cannot find \"${client}\" in ${machines}.\nPlease check that it is a CSV file." >&2
        exit 255
    fi
    echo "${userip}"
}

get_clientpass() {
    local machines=${CONFIG_DIR}/tables/CLIENTS.csv
    if ! [ -e $machines ]; then
        echo "Cannot find the CLIENTS.csv file at $CONFIG_DIR"
        exit 255
    fi
    local client="$1"
    local colname="NeedPass"
    local colnum=$(head -1 $machines | grep -oP ".*${colname}" | grep -o , | wc -l)
    colnum=$(( $colnum + 1 ))
    local pass=$(grep "^${client}," $machines | cut -d, -f $colnum | grep -v "${colname}")
    if [ "$pass" = "" ]; then
        echo -e "Cannot find \"${client}\" in ${machines}.\nPlease check that it is a CSV file." >&2
        exit 255
    fi
    echo "${pass}"
}

get_cronfile() {
    local crondir=${CONFIG_DIR}/generated_cron/
    local client="$1"
    local cronfile="${crondir}/${client}.cron"
    if ! [ -e $cronfile ]; then
        echo -e "Cannot find \"${client}\" in ${crondir}.\nPlease check that it has a '.cron' extension." >&2
        exit 255
    fi
    echo "$cronfile"
}

generate_server_crontab() {
    local client="$1"
    local file="$2"
    local outfile=$(mktemp).cron
    mkdir -p $(dirname $outfile)
    local this=${REPO_DIR}/server/run-scripts/send_script.sh
    awk -v scr=${this} -v clt=${client} '{print $1" "$2" "$3" "$4" "$5" "scr" "clt}' ${file} > ${outfile}
    if ! [ -e $outfile ]; then
        echo "Unable to generate: $outfile" >&2
        exit 255
    fi
    echo "$outfile"
}

update_crontab_ifneeded(){
    local client="$1"
    local cronfile="$2"
    local update_existing="$3"  ## blank, clear or update
    local has_crondata=`crontab -l | grep ${client}`

    if [ "${has_crondata}" = "" ] || [ "${update_existing}" != "" ]; then
        ## if no existing entry or it is told to update:
        # - Generate the actual crontab the server will run, and append/replace in new
        local client_to_server_cron=$(generate_server_crontab ${client} ${cronfile}) || exit 255
        local tmp_cron=/tmp/server.cron
        crontab -l > ${tmp_cron}

        # this "clears" for a client
        if [ "${update_existing}" != "" ]; then
            grep -v ${client} ${tmp_cron} > ${tmp_cron}.without
            mv ${tmp_cron}.without ${tmp_cron}
        fi
        # this "updates" for a client, otherwise if it's clear, then it's not included
        if [ "${update_existing}" = "update" ]; then
            cat ${client_to_server_cron} >> ${tmp_cron}
        fi
        log_this $client "$update_existing"
        crontab -r
        crontab ${tmp_cron} || echo "Bad crontab! See:  ${tmp_cron}" && exit 255
    fi

}