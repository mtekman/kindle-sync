#!/usr/bin/env python

try:
    from crontab import CronTab
except ImportError:
    print("Please 'pip3 install crontab'")
    exit(-1)

from datetime import datetime, timedelta
from sys import argv

command_map = {}    
list_of_prevtimes = []
list_of_nexttimes = []
list_of_nextnexttimes = []
delim=' '

cron_file = argv[1]
with open(cron_file, 'r') as f:
    for line in f:
        if len(line) < 5:continue
        tokens = line.split()
        ct = delim.join(tokens[:5])
        comm = delim.join(tokens[5:])
        ctobj = CronTab(ct)
        prev_seconds = ctobj.previous(default_utc=True)
        next_seconds = ctobj.next(default_utc=True)
        ## Next, next!
        newnow = datetime.now() + timedelta(seconds=next_seconds)
        next_next_seconds = ctobj.next(now=newnow, default_utc=True)
        ## Append both
        list_of_prevtimes.append(int(prev_seconds))
        list_of_nexttimes.append(int(next_seconds))
        list_of_nextnexttimes.append(int(next_seconds + next_next_seconds))
        command_map[int(prev_seconds)]=comm

last_one = max(list_of_prevtimes)
curr_one = min(list_of_nexttimes)
next_one = min(list_of_nextnexttimes)

print("Prev Times:", last_one, list_of_prevtimes)
print("Curr Times:", curr_one, list_of_nexttimes)
print("Next Times:", next_one, list_of_nextnexttimes)

print("Will Execute: %s, Last: %d, Next: %d " % (command_map[last_one], last_one, curr_one))
