#!/usr/bin/env python3

from sys import argv, stderr
from os.path import join

try:
    import pandas as pd
except ImportError:
    print("Please install python pandas")
    exit(-1)

time_map = {}; # machine -> command -> hours    ##Morning :var t2=Afternoon :var t3=Evening

if len(argv) <= 5:
    print('''Generates separate crontabs for each client, grouped by common commands.
The crontabs produced here are not to be used for actual cron, but to be parsed by the next-wakeup.py script.

    crontab-generator.py <media_script> <output_folder> <machtable> <commtable> <[timetable1 [timetable2 ... ]]>

where:
    media_script   is the full path to the makefetch_media script
    output_folder  is the path to the output folder where the generated cron 
                   for each client is written
    machtable      a table with columns `Name' `User@IP'
    commtable      a table with columns `Name' `Command' `Notes'
    timetable      one or many tables with columns `<day-range>' `<hour1>' `<hour2>' …

where each table is a csv-separated file.

    ''', file=stderr)
    exit(-1)

media_script=argv[1]
out_folder=argv[2]
file_machines = argv[3]
file_commands = argv[4]
file_timetables = argv[5:]

table_mach = pd.read_csv(file_machines, keep_default_na=False)
table_comm = pd.read_csv(file_commands, keep_default_na=False)

def table_to_map(table):
    table_data = pd.read_csv(table, keep_default_na=False)
    days = table_data.columns[0]
    # - 50
    for mname in table_data.index:
        row = table_data.loc[mname]
        machine = (row.iloc[0], days)
        if machine not in time_map:
           time_map[machine] = {}  # commands -> hours
        hours = list(filter(lambda x: x != "Notes", row.index))[1:]
        for hour in hours:
            command=row[hour]
            if command.strip()=="":continue
            if command not in time_map[machine]:
               time_map[machine][command] = []
            time_map[machine][command].append(hour)

def populate_map_from_tables():
    #table_to_map(mo);table_to_map(af);table_to_map(ev);table_to_map(we)
    for tab in file_timetables:
        ##print("Parsing time table:", tab, file=stderr)
        table_to_map(tab)

def get_machine(name):
    return(table_mach.loc[table_mach["Name"] == name]["User@IP"].values[0])

def get_command(tag):
    return(table_comm.loc[table_comm["Name"] == tag]["Command"].values[0])

def make_single_crontab(machine, days, commtag, hours, white_map):
    comm = get_command(commtag)
    comm_format="%%-%ds %%-%ds %%s" % (
        white_map["time"], ##white_map["day"],
        len(media_script)
    )
    comm_string = comm_format % (
        "0 " + (",".join([str(int(x)) for x in hours])) + " * * " + days,
        media_script, comm
    )
    return(comm_string)

def make_single_crontab_minutes(machine, commtag, minutes, white_map):
    comm = get_command(commtag)
    comm_format="%%-%ds %%-%ds %%s" % (
        white_map["time"],
        len(media_script)
    )
    comm_string = comm_format % (
        ",".join([str(int(x)) for x in minutes]) + " * * * *",
        media_script, comm
    )
    return(comm_string)

##return((machine, commtag, white_map))

def set_whitespace_formatting():
    # Get maximum formatting
    max_time = max_day = max_mach = 0
    white_map = {'all' : {'time':0, 'day':0, 'mach':0}}
    for mach_day in time_map:
        mach, day = mach_day
        if mach not in white_map:
            white_map[mach] = {'time':0, 'day':len(day), 'mach':len(mach)}
        if len(mach) > white_map['all']['mach']:
            white_map['all']['mach']=len(mach)            
        if len(day) > white_map['all']['day']:
            white_map['all']['day']=len(day)
        for comm in time_map[mach_day]:
            min_or_hours = time_map[mach_day][comm]
            time_form = ",".join([str(int(x)) for x in min_or_hours])
            if day == "Minutes":
                time_form = time_form + " * * * *"
            else: ## it's hours
                time_form = "0 " + time_form + " * * " + day
            # Update max whitespace for all
            if len(time_form) > white_map['all']['time']:
                white_map['all']['time']=len(time_form)
            # Update max whitespace for machine only
            if len(time_form) > white_map[mach]['time']:
                white_map[mach]['time']=len(time_form)
    return(white_map)

def make_all_crontabs():
    white_map = set_whitespace_formatting()
    print("[cronjob] updated", out_folder, flush=True, end=": ")
    first_mach = {}
    for mach_day in time_map:
        ## Note that the same mach can appear in the time_map multiple times
        ## since it can be paired with different days.
        ## On the first write we overwrite the file, on subsequent, we append.
        mach, day = mach_day
        if mach not in first_mach:
            first_mach[mach] = 0
        first_mach[mach] += 1
        mac_cron = join(out_folder, mach + ".cron")
        file_mode = 'w' if first_mach[mach] == 1 else 'a'
        with open(mac_cron, file_mode) as f:
            for commtag in time_map[mach_day]:
                times = time_map[mach_day][commtag]
                if day == "Minutes":
                    print(make_single_crontab_minutes(mach, commtag, times,
                                                      white_map[mach]), file=f)
                else:
                    print(make_single_crontab(mach, day, commtag, times,
                                              white_map[mach]), file=f)
        if file_mode == 'w':
            print(mach, end=" ", flush=True)
    print("")
 
                    
populate_map_from_tables()
make_all_crontabs()
