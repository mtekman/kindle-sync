#!/bin/bash

# Dynamic variables
CLIENT_NAME=${CLIENT_NAME:-AboveComp}

# if [ "$3" = "" ]; then
#     echo "
#     `basename $0` <client-name> <client-user-and-ip> <image-path>

# Send IMAGE-PATH to machine named CLIENT_NAME at CLIENT-USER-AND-IP.

#    e.g. `basename $0` MyKindle root@192.168.0.2 ~/test.png

# The CLIENT_NAME is whatever you want it to be, it is not related to
# the hostname of the client, merely a moniker for the logs."
#     exit 255
# fi

# Fixed variables
CLIENT_INSTALL_DIR=${CLIENT_INSTALL_DIR:-/mnt/base-us/kindle-sync/}
CLIENT_SENTMEDIA_DIR=${CLIENT_INSTALL_DIR}/sent/
CLIENT_IMAGE_FILE=${CLIENT_SENTMEDIA_DIR}/synced-image.png
CLIENT_AUDIO_FILE=${CLIENT_SENTMEDIA_DIR}/synced-audio.wav
CLIENT_LAST_LINE=39
#-
SERVER_LOG_FILE=${SERVER_LOG_FILE:-~/kindle-log.txt}

DATE_FMT="+%Y-%m-%d|%H:%M:%S"
SSH_TIMEOUT=10
SSH_OPTS="-o ConnectTimeout=${SSH_TIMEOUT}"
SSH_MULTIPLEX="yes" ## if multiplexed, we get *way* faster re-connections
if [ "$SSH_MULTIPLEX" = "yes" ];then
    SSH_OPTS="${SSH_OPTS} -o ControlPersist=1m -o ControlMaster=auto -o ControlPath=\"~/.ssh/conmast%r@%h:%p\""
fi

# - These exist in auxillary.sh
# date_cmd() {
#     date "$DATE_FMT" "$@"
# }

# log_this() {
#     echo "$(date_cmd) | $CLIENT_NAME | $*"  >> $SERVER_LOG_FILE
# }

SCP_CMD() {
    local src="$1"
    local dst="$2"
    if [ "${CLIENT_USER_IP}" = "" ]; then
        echo "No IP detected" >&2 && exit -1
    fi
    if [ "${CLIENT_PASS}" != "" ]; then
        sshpass -p "${CLIENT_PASS}" scp ${src} ${dst};
    else
        scp ${SSH_OPTS} -o PasswordAuthentication=no "${src}" "${dst}"
    fi
}

SSH_CMD() {
    local args="$@"
    if [ "${CLIENT_USER_IP}" = "" ]; then
        echo "No IP detected" >&2 && exit -1
    fi
    if [ "${CLIENT_PASS}" != "" ]; then
        sshpass -p "${CLIENT_PASS}" ssh ${SSH_OPTS} -o PasswordAuthentication=yes \
                ${CLIENT_USER_IP} ${args}
    else
        ssh ${SSH_OPTS} -o PasswordAuthentication=no ${CLIENT_USER_IP} ${args}
    fi
}


server2client_isitup(){
    SSH_CMD "/bin/true" 2>/dev/null
}

server2client_synctime() {
    SSH_CMD "date -s @`(date -u +"%s" )`"
}

server2client_killjobs(){
    SSH_CMD "tmux kill-server; pkill -f tmux;"
    log_this "killjobs" "tmux jobs deleted"
}

server2client_sendimage() {
    local image=$1
    if [ "${image}" = "" ]; then
        log_this "sendimage" "No image"
        return 0
    fi
    # - copy the image
    log_this "sendimage" ${image}
    SCP_CMD ${image} ${CLIENT_USER_IP}:${CLIENT_IMAGE_FILE}
    log_this "sendimage" "sent "`SSH_CMD "/usr/sbin/eips -f -g ${CLIENT_IMAGE_FILE}"`
}

server2client_sendaudio() {
    local wav=$1
    if [ "${wav}" = "" ]; then
        log_this "sendaudio" "No audio"
        return 0
    fi
    # - copy the wav
    log_this "sendaudio" ${wav}
    SCP_CMD ${wav} ${CLIENT_USER_IP}:${CLIENT_AUDIO_FILE}
    # - initctl audio must be enabled first, so run this before the kill script.
    SSH_CMD "aplay ${CLIENT_AUDIO_FILE}"
    log_this "sendaudio" "sent"
}

server2client_sendmessage() {
    local mess="$1"
    if [ "${mess}" = "" ]; then
        log_this "sendmessage" "No message"
        return 0
    fi
    local xcord=${2:-0}
    local ycord=${3:--1}  # if a ycord is negative, then count from last line
    local delay=${4:--1}  # if a delay of -1, then never remove the message.

    if [ $ycord -lt 0 ]; then
        ycord=$(( ${CLIENT_LAST_LINE} + ${ycord} + 1 ))
    fi
    log_this "sendmessage" "x=$xcord y=$ycord delay=$delay"
    local blank=$(echo $mess | sed 's|.| |g')
    SSH_CMD "eips ${xcord} ${ycord} \"${blank}\""   ## blank out background first
    if [ $delay = -1 ]; then
        SSH_CMD "eips ${xcord} ${ycord} \"${mess}\""
    else
        SSH_CMD "eips ${xcord} ${ycord} \"${mess}\" && sleep ${delay} && eips ${xcord} ${ycord} \"${blank}\""
    fi
    log_this "sendmessage" "sent"
}

server2client_puttosleep() {
    local next_wakeup_secs=$1
    local delay=${2:-0}
    [ "$next_wakeup_secs" = "" ] && echo "No duration given" && exit 255
    next_wakeup_secs=$(( $next_wakeup_secs - $delay ))
    ##local comm_sleep="bash /mnt/base-us/sleeptest.sh ${next_wakeup_secs}"
    local comm_sleep="bash /mnt/base-us/run_sleep.sh ${next_wakeup_secs}"
    SSH_CMD "tmux kill-session -t lin; tmux new -d -s lin \"sleep $delay; ${comm_sleep}\""
    log_this "puttosleep" "Next wakeup: "$(date_cmd --date "$next_wakeup_secs seconds")" in ${next_wakeup_secs} seconds"
}

server2client_waitforclient() {
    local maxtime=${1:-10 mins}  # max wait is 10 minutes from now
    local waitdelay=${2:-10}     # wait 10 seconds between tries

    if ! [[ $maxtime =~ mins ]]; then
        log_this "waitforclient" "Must use <X mins> and <Y Seconds>"
        exit 255
    fi
    local maxsecs=$(date --date "$maxtime" "+%s")
    while ! server2client_isitup; do
        local secsnow=$(date "+%s")
        if [ $secsnow -gt $maxsecs ]; then
            log_this "waitforclient" "Cannot connect, max time of $maxtime exceeded, bailing."
            exit 255    # quits the whole script
        fi
        log_this "waitforclient" "Cannot connect to ${CLIENT_NAME}, trying again in $waitdelay seconds"
        sleep $waitdelay         #- wait 10 seconds
    done
}

server2client_getbattery() {
    local batt=$(SSH_CMD "gasgauge-info -c" | sed 's/%//')
    log_this "getbattery" "Battery $batt" >&2 2>/dev/null
    echo $batt
}

# -- MAIN -- #
server2client_mainloop() {
    CLIENT_NAME="$1"
    CLIENT_USER_IP=$(get_userip ${CLIENT_NAME}) || exit 255

    local img="$2"
    local wav="$3"

    local sleep_for="$4"
    local wakeup_early=${5:-10}
    ## accurate to nearest minute (rounded up)
    local timestamp=$(date "+%Y-%m-%d %H:%M" --date "$(( ${next_time} + 30 )) seconds")
    ## time to actually wake up client
    local next_time=$(( $next_time - ${wakeup_early} ))

    server2client_waitforclient "10 mins"     ## max wait time
    server2client_synctime
    server2client_killjobs
    server2client_sendimage ${img}
    server2client_sendaudio ${wav}
    batt=$(server2client_getbattery)
    server2client_sendmessage "next:${timestamp}  batt:${batt}" 1
    # when this wakes up, it also waits for wifi
    if [ ${sleep_for} -gt 1 ]; then
        server2client_puttosleep ${sleep_for} 5
    fi
}
