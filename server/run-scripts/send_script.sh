#!/bin/bash

if [ "$1" = "" ];then echo "

Sends media to a client of choice, and schedules itself for the
next (and the next).

    `basename $0` <client-name> [--update-cron]

where:
 client-name    is part of the CLIENTS.csv
 update-cron    forces an update of the cronjob for that client
" && exit 255
fi

CLIENT_NAME="$1"
update_cron="$2"  ## blank, clear, or update

REPO_DIR=${REPO_DIR:-~/repos/_mtekman/kindle-sync}
CONFIG_DIR=${CONFIG_DIR:-~/repos/_mtekman/kindle-sync/config}

SERVER_LOG_FILE=${CONFIG_DIR}/logs/client_log.${CLIENT_NAME}.txt
mkdir -p $(dirname $SERVER_LOG_FILE)
touch $SERVER_LOG_FILE

# - Load source
SUB_SCRIPT_DIR=${REPO_DIR}/server/run-scripts/submodules
source ${SUB_SCRIPT_DIR}/auxillary.sh
source ${SUB_SCRIPT_DIR}/server2client.sh

#- This cron file describes the client, but is not actually directly used.
#  Instead we generate a server cron file based on its contents.
client_cron_file=$(get_cronfile ${CLIENT_NAME}) || exit 255

#- Find out what the next command in the crontab is, and how long to wait for it.
next_wakeup=$(python ${SUB_SCRIPT_DIR}/next-wakeup.py ${client_cron_file})
next_comm=$(echo "$next_wakeup" | grep "Will Execute:" \
                | sed -r 's|Will Execute: (.+), Last: ([0-9-]+), Next: ([0-9-]+).*|\1|')
next_time=$(echo "$next_wakeup" | grep "Will Execute:" \
                | sed -r 's|Will Execute: (.+), Last: ([0-9-]+), Next: ([0-9-]+).*|\3|')
#- Run command and get media items
comm_out=$(eval "$next_comm")
get_img=$(echo "$comm_out" | grep "IMG:" | sed -r 's|IMG: ([^,]*).*|\1|')
get_wav=$(echo "$comm_out" | grep "WAV:" | sed -r 's|.*WAV: (.*)|\1|')

#- Generate and install crontab if client not present, or if update needed
update_crontab_ifneeded ${CLIENT_NAME} ${client_cron_file} ${update_cron}
#- Send the data to the client, quotes are a REQUIREMENT in case img or wav is missing.
server2client_mainloop "${CLIENT_NAME}" "${get_img}" "${get_wav}" "${next_time}" 10 
