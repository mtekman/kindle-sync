SERVER_SSH_DIR=~/.ssh

server2client_assertitisup() {
    if ! server2client_isitup; then
        local rsa_pub=$(find $SERVER_SSH_DIR -type f -name "id_rsa*.pub" | grep dot.ssh)
        echo ""
        if [ "${rsa_pub}" = "" ]; then
            echo "No rsa key found. Please run 'ssh-keygen' and try again. It should install to ${SERVER_SSH_DIR}"
            exit 255
        fi
        echo "It looks like we cannot copy to ${CLIENT_USER_IP} without a password."
        echo "Please ensure that you have copied the contents of:"
        echo "             cat ${rsa_pub}"
        echo "to the remote file:"
        echo "             ${CLIENT_USER_IP}:/mnt/us/usbnet/etc/authorized_keys"
        echo ""
        echo "Or to configure and copy keys, run:"
        echo "       cat ${SERVER_SSH_DIR}/id_rsa.pub | ssh ${CLIENT_USER_IP} 'sh /mnt/base-us/kindle-sync-client/initialise_auth_keys.sh; cat >> /mnt/base-us/usbnet/etc/authorized_keys'"
        exit 255
    fi
}


configure_client_ssh_for_easy_access(){
    local user_ip=${USER}"@"$(echo ${SSH_CONNECTION} | cut -d' ' -f 3)
    if [ "${user_ip}" != "${CLIENT_USER_IP}" ]; then
        echo "This function MUST be called on the client, from the server"
        echo "   cat ${SERVER_SSH_DIR}/id_rsa.pub | ssh ${CLIENT_USER_IP} 'sh /mnt/base-us/dashboard/initialise_auth_keys.sh; cat >> /mnt/base-us/usbnet/etc/authorized_keys'"
        echo ""
        exit -1
    fi
    # Tell kindle where ssh is to look for authorized keys.
    local USBNET_DIR="/mnt/base-us/usbnet/"
    local AUTHOR_KEY="${USBNET_DIR}/etc/authorized_keys"
    local SSHD_CONFG="${USBNET_DIR}/etc/config"
    # - Authorized Keys Initialise
    echo "[*] Configuring SSHD file to point to Authorized Keys"
    mkdir -p ${USBNET_DIR} 2>/dev/null
    sed -i 's|^\#AuthorizedKeysFile.*|AuthorizedKeysFile ${AUTHOR_KEY}|' ${USBNET_DIR}/etc/sshd_config &&
        touch ${AUTHOR_KEY} &&
        echo "[*] Configuring USBNET file to enable SSHD on boot"
    ## Configure SSH to always start on reboot
    sed -i 's|^USE_WIFI=.*|USE_WIFI="true"|' ${SSHD_CONFG} &&
        sed -i 's|^USE_OPENSSH.*|USE_OPENSSH="true"|' ${SSHD_CONFG} &&
        sed -i 's|^QUIET_DROPBEAR.*|QUIET_DROPBEAR="false"|' ${SSHD_CONFG} &&
        sed -i 's|^TWEAK_MAC_ADDRESS.*|TWEAK_MAC_ADDRESS="false"|' ${SSHD_CONFG} &&
        touch ${USBNET_DIR}/auto &&
        echo "" > ${USBNET_DIR}/DISABLED_AUTO &&  ## newline needed?
        initctl restart usbnetd &&
        initctl restart usbnet &&
        sleep 5 &&
        initctl restart usbnet   ## need to restart twice for someone
}


configure_client_iptables_for_strict_access(){
    # - This needs to be called every time the device resets.
    server2client_assertitisup
    local file="/mnt/base-us/ksync.configure_iptables.runonboot.sh"
    # - Get ip range
    local ip_range=$(run_ssh "ip a show dev wlan0 | awk '/^\s+inet\s+/ {split(\$2,a,\".\");split(a[4],b,\"/\");print a[1]\".\"a[2]\".\"a[3]\".1/\"b[2]}'")
    [ "$ip_range" = "" ] && echo "Could not determine LAN IP range." && exit -1
    # - Set the rules
    echo "
# Flush old rules, old custom tables
iptables --flush
iptables --delete-chain

# Set default policies for all three default chains
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

# Enable free use of loopback interfaces
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# All TCP sessions should begin with SYN
iptables -A INPUT -p tcp ! --syn -m state --state NEW -s ${ip_range} -j DROP

# Accept inbound TCP packets
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -s ${ip_range} -j ACCEPT

# Accept outbound packets
iptables -I OUTPUT 1 -m state --state RELATED,ESTABLISHED -j ACCEPT
##iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT   ## allow DNS?
" | run_ssh "cat - > ${file}" && echo "Copied over: iptables script"
    run_ssh "tmux -c \"sh ${file}\"" && echo "Script Run, current table:" &&
        run_ssh "iptables -L -v -n"
}

init(){
    # - Configure SSH, IPtables, disable audio.
    configure_client_ssh_for_easy_access
    configure_client_iptables_for_strict_access
    configure_client_services
}

run(){
    # - client should be waking up
    server2client_waitforclient
    # - send image and put client to sleep
    server2client_synctime
    server2client_sendimage $IMAGE_TO_SEND
    server2client_getbattery
    server2client_puttosleep
}

