#!/bin/bash
# - Heavily inspired by:
# https://github.com/nicoh88/kindle-kt3_weatherdisplay_battery-optimized/blob/master/Kindle/weatherscript.sh

server_calculated_sleep_time=${1:-120}   # determined by ssh command from server
TESTIP=REPLACE_ON_INSTALL_BY_SCRIPT  # "192.168.0.178"

# -- THESE FOLDERS AND IMAGES SHOULD EXIST ALREADY
#    If not, make sure you ran in the "client_install.sh" script.
KIN_DIR=/mnt/base-us/kindle-sync/
LOG=${KIN_DIR}/log.txt
IMG_DIR=${KIN_DIR}/images/
SENT_DIR=${KIN_DIR}/sent/  ## not used by this script, but good to know it exists.

# Every N screen updates (or reboot), run certain scripts:
# e.g. iptables, kill services, cpu regulation
#  essentially anything that should ideally only be run once, but
#  likely needs to be run many times in case settings revert
RUNON_UPDATE=10

ASS_DIR=${IMG_DIR}/assets/
img_nobattery=${ASS_DIR}/nobattery.png
img_noconnect=${ASS_DIR}/noconnect.png
img_waiting=${ASS_DIR}/waiting.png
img_poweroff=${ASS_DIR}/poweroff.png
# --

goto_sleep(){
    local duration=${1:-120}
    ## local next_time=$(date -d $(( $(date +%s) + ${duration} ))) ## broken, epoch is just wrong
    echo "`date '+%Y-%m-%d_%H:%M:%S'` | Start Sleep | Waiting $duration seconds" >> ${LOG}
    ## ignore 1970 message
    rtcwake -l -d /dev/rtc1 -m no -s $duration
    echo "mem" > /sys/power/state
}

kill_kindle(){
    initctl -q;
    if [ $? = 127 ]; then
        kill_kindle_k3gb
    else
        kill_kindle_ktouch
    fi
}

kill_kindle_k3gb(){
    echo "Killing Kindle Jobs via /etc/init.d"
    /etc/init.d/audio stop         > /dev/null 2>&1
    /etc/init.d/cmd stop           > /dev/null 2>&1
    /etc/init.d/otaupd stop        > /dev/null 2>&1
    /etc/init.d/webreader stop     > /dev/null 2>&1
    /etc/init.d/browserd stop      > /dev/null 2>&1      # not exist 5.9.4
    /etc/init.d/framework stop     > /dev/null 2>&1      # "powerd_test -p" doesnt work, other command found
    /etc/init.d/todo stop          > /dev/null 2>&1
    /etc/init.d/blanket stop       > /dev/null 2>&1
    /etc/init.d/x stop             > /dev/null 2>&1      # frees up a lot of ram
    ## initctl stop volumd        > /dev/null 2>&1 -- needed for usbnet to function I think
    /etc/init.d/phd stop           > /dev/null 2>&1
    killall lipc-wait-event    > /dev/null 2>&1
    ##initctl stop tmd          > /dev/null 2>&1
    ##initctl stop volumd       > /dev/null 2>&1      # - No! Needed by usbnet
    ##initctl stop powerd       > /dev/null 2>&1      # battery state doesnt work
    ##initctl stop lab126       > /dev/null 2>&1      # wlan interface doesnt work
    ##initctl stop pmond        > /dev/null 2>&1      # not exist 5.9.4
}

kill_kindle_ktouch(){
    echo "Killing Kindle Jobs via initctl"
    initctl stop audio         > /dev/null 2>&1
    initctl stop cmd           > /dev/null 2>&1
    initctl stop otaupd        > /dev/null 2>&1
    initctl stop webreader     > /dev/null 2>&1
    initctl stop browserd      > /dev/null 2>&1      # not exist 5.9.4
    initctl stop framework     > /dev/null 2>&1      # "powerd_test -p" doesnt work, other command found
    ##/etc/init.d/framework stop > /dev/null 2>&1  # just in case
    initctl stop todo          > /dev/null 2>&1
    initctl stop blanket       > /dev/null 2>&1
    initctl stop x             > /dev/null 2>&1      # frees up a lot of ram
    ## initctl stop volumd        > /dev/null 2>&1 -- needed for usbnet to function I think
    initctl stop phd           > /dev/null 2>&1
    killall lipc-wait-event    > /dev/null 2>&1
    ##initctl stop tmd          > /dev/null 2>&1
    ##initctl stop volumd       > /dev/null 2>&1      # - No! Needed by usbnet
    ##initctl stop powerd       > /dev/null 2>&1      # battery state doesnt work
    ##initctl stop lab126       > /dev/null 2>&1      # wlan interface doesnt work
    ##initctl stop pmond        > /dev/null 2>&1      # not exist 5.9.4
}


enable_cpu_powersave(){
    local CHECKCPUMODE=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor | grep -i "powersave"`
    if [ ${CHECKCPUMODE} -eq 0 ]; then
        echo powersave > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | CPU runtergetaktet." >> ${LOG} 2>&1
    fi
}

disable_screensaver(){
    # powerd buggy since 5.4.5 - https://www.mobileread.com/forums/showthread.php?t=235821
    local CHECKSAVER=`lipc-get-prop com.lab126.powerd status | grep -i "prevent_screen_saver:0"`
    if [ ${CHECKSAVER} -eq 0 ]; then
        lipc-set-prop com.lab126.powerd preventScreenSaver 1 >> ${LOG} 2>&1
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | Standard Energiesparmodus deaktiviert." >> ${LOG} 2>&1
    fi
}

get_battery(){
    local CHECKBATTERY=`gasgauge-info -s | sed 's|%$||'`
    echo "`date '+%Y-%m-%d_%H:%M:%S'` | Batteriezustand: ${CHECKBATTERY}%" >> ${LOG} 2>&1

    if [ ${CHECKBATTERY} -le 1 ]; then
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | Batteriezustand 1%, statisches Batteriezustandsbild gesetzt, WLAN deaktivert, Ruhezustand!" >> ${LOG} 2>&1
        eips -f -g $img_nobattery
        lipc-set-prop com.lab126.wifid enable 0
        echo 0 > /sys/class/rtc/rtc0/wakealarm
        echo "mem" > /sys/power/state
    fi
}

wlan_isnotup() {
  return `lipc-get-prop com.lab126.wifid cmState | grep CONNECTED | wc -l`
}

wait_for_wlan(){
    ##lipc-set-prop com.lab126.cmd wirelessEnable 1
    local WLANNOTCONNECTED=0
    local WLANCOUNTER=0
    while wlan_isnotup; do
        if [ ${WLANCOUNTER} -eq 10 ] || [ ${WLANCOUNTER} -eq 30 ] || [ ${WLANCOUNTER} -eq 50 ] || [ ${WLANCOUNTER} -eq 60 ]; then
            echo "" >> ${LOG} 2>&1
            echo "## DEBUG BEGIN" >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | DEBUG ifconfig > `ifconfig wlan0`" >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | DEBUG wifid cmState > `lipc-get-prop com.lab126.wifid cmState`" >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | DEBUG wifid signalStrength > `lipc-get-prop com.lab126.wifid signalStrength`" >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | DEBUG wpa_cli status > `wpa_cli status verbose`" >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | DEBUG ping ${TESTIP} > `ping ${TESTIP} -c4`" >> ${LOG} 2>&1
            echo "## DEBUG END" >> ${LOG} 2>&1
            echo "" >> ${LOG} 2>&1
        fi
        if [ ${WLANCOUNTER} -eq 10 ]; then
            wpa_cli -i wlan0 disconnect >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | WLAN disconnect." >> ${LOG} 2>&1
            wpa_cli -i wlan0 reconnect >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | WLAN reconnect." >> ${LOG} 2>&1
        fi
        if [ ${WLANCOUNTER} -eq 30 ]; then
            lipc-set-prop com.lab126.wifid enable 0 >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | WLAN deaktivieren." >> ${LOG} 2>&1
            lipc-set-prop com.lab126.wifid enable 1 >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | WLAN aktivieren." >> ${LOG} 2>&1
        fi
        if [ ${WLANCOUNTER} -eq 50 ]; then
            wpa_cli -i wlan0 reassociate >> ${LOG} 2>&1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | WLAN reassociate." >> ${LOG} 2>&1
        fi
        if [ ${WLANCOUNTER} -eq 60 ]; then
            eips -f -g $img_noconnect
            WLANNOTCONNECTED=1
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | Leider keine erfolgreiche Verbindung mit einem WLAN hergestellt." >> ${LOG} 2>&1
            break
        fi
        let WLANCOUNTER=WLANCOUNTER+1
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | Warte auf WLAN (Versuch ${WLANCOUNTER})." >> ${LOG} 2>&1
        sleep 1
    done
    echo "`date '+%Y-%m-%d_%H:%M:%S'` | Connected after ${WLANCOUNTER} tries" >> ${LOG} 2>&1
}


run_scripts() {
    for script in "$@"; do
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | Ran: $script" >> ${LOG} 2>&1
        $script
    done
}

run_on_count() {
    local count_file="/tmp/kindle.counter"
    local max_update="$1"
    shift 1
    local scripts="$*"
    if [ "$scripts" = "" ]; then
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | No scripts given" >> ${LOG} 2>&1
        return -1
    fi
    local count=0
    if [ -e $count_file ]; then
        count=$(cat $count_file)
        if [ $count -gt $max_update ]; then
            count=0 # reset
            echo "`date '+%Y-%m-%d_%H:%M:%S'` | $ run" >> ${LOG} 2>&1
            run_scripts $scripts
        else
            count=$(( count + 1 ))
        fi
    else
        ## likely a fresh reboot
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | $name run at new boot" >> ${LOG} 2>&1
        run_scripts $scripts
    fi
    echo -n $count > $count_file
}

wait_and_bleed (){
    #- Upon waking do: Every 30 minutes, try to enable wifi usually the machine will be asleep so
    #                  it's not that wasteful
    #- By the time this function is called, we have already waited 1min for wifi
    echo "`date '+%Y-%m-%d_%H:%M:%S'` | Loopnum = $LOOPNUM" >> ${LOG} 2>&1
    let LOOPNUM=LOOPNUM+1
    if [ ${LOOPNUM} -gt 2 ]; then
        echo "`date '+%Y-%m-%d_%H:%M:%S'` | More than 1 hour waiting with Wifi, shutting down client to protect battery" >> ${LOG} 2>&1
        eips -f -g $img_poweroff && /sbin/shutdown -P now
    fi
    ## Wait 1 more minute before the changing the screen, and then after you do, sleep for 30 mins.
    sleep 60 && eips -f -g $img_waiting && sleep 1800;
}


#---- M A I N ----#

#- Sleep for amount
goto_sleep $server_calculated_sleep_time
#- Upon waking up, do:
while :; do
    #- Run this every N updates or on boot
    run_on_count ${RUNON_UPDATE} \
                 kill_kindle \
                 iptables \
                 disable_screensaver \
                 enable_cpu_powersave
    #- Run this every update
    get_battery
    wait_for_wlan
    #- Finally, wait for signal or, failing that, sleep and try again
    wait_and_bleed
done
