#!/bin/bash

ip_range=$(ip a show dev wlan0 | awk '/^\s+inet\s+/{\
split($2,a,".");\
split(a[4],b,"/");\
print a[1]"."a[2]"."a[3]".1/"b[2]\
}')
[ "$ip_range" = "" ] && echo "Could not determine LAN IP range." && exit 255

iptables=/usr/sbin/iptables

# Flush old rules, old custom tables
$iptables --flush
$iptables --delete-chain

# Set default policies for all three default chains
$iptables -P INPUT DROP
$iptables -P FORWARD DROP
$iptables -P OUTPUT DROP

# Enable free use of loopback interfaces
$iptables -A INPUT -i lo -j ACCEPT
$iptables -A OUTPUT -o lo -j ACCEPT

# All TCP sessions should begin with SYN
$iptables -A INPUT -p tcp ! --syn -m state --state NEW -s ${ip_range} -j DROP

# Accept inbound TCP packets
$iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
$iptables -A INPUT -p tcp --dport 22 -m state --state NEW -s ${ip_range} -j ACCEPT

# Accept outbound packets
$iptables -I OUTPUT 1 -m state --state RELATED,ESTABLISHED -j ACCEPT
##iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT   ## allow DNS?

# To list all rules:
#    iptables -L -v -n 