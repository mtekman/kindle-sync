#!/bin/bash

set -e

REPO_DIR=${REPO_DIR:-~/repos/_mtekman/kindle-sync}
CLIENT_INSTALL_DIR=${CLIENT_INSTALL_DIR:-/mnt/base-us/kindle-sync/}
SERVER_SSH_DIR=~/.ssh
#--
CLIENT_USER_IP=$1
SERVER_IP=${2:-$(ip route get 1 | grep -oP "via [0-9.]+" | cut -d' ' -f 2)}
CLIENT_PASS="$3"

if [ "$CLIENT_USER_IP" = "" ] ||  [ "$SERVER_IP" = "" ];then
    echo "
This script installs the client scripts to the device.

The server ip address should be supplied also so that the client knows
which address to probe when testing LAN connectivity. If not, the
active connection address is taken. 

If client-pass is given, then tcl's expect script is used.

  $(basename $0) <user@client-ip-address> <server-ip-address> [client-pass]

" && exit 255

fi

source ${REPO_DIR}/server/run-scripts/submodules/server2client.sh

ssh_test() {
    SSH_CMD "/bin/true 2>/dev/null"
    [ $? == 0 ] && echo "yes" || echo "no"
}

attempt_to_ssh() {
    if [ "$(ssh_test)" = "no" ]; then
        local rsa_pub=$(find ${SERVER_SSH_DIR} -type f -name "id_rsa*.pub")
        if [ "${rsa_pub}" = "" ]; then
            echo "No RSA key found in ${SERVER_SSH_DIR}. Please run 'ssh-keygen' and try again."
            exit 255
        else
            # embed RSA key within the initialise_auth_key script to send over.
            #- extract and install RSA pub key
            #- patch usbnet
            tmpfile=$(mktemp)
            cp $REPO_DIR/client/helpers/initialise_auth_keys.sh $tmpfile &&
                echo "" >> $tmpfile
                cat ~/.ssh/id_rsa.pub | sed 's|^|###|' >> $tmpfile &&
                cat $tmpfile | SSH_CMD ${CLIENT_USER_IP} "\
mkdir ${CLIENT_INSTALL_DIR} 2>/dev/null;\
cat > ${CLIENT_INSTALL_DIR}/initialise_auth_keys.sh;\
tail -1 ${CLIENT_INSTALL_DIR}/initialise_auth_keys.sh | sed 's|^###||' > /mnt/base-us/usbnet/etc/authorized_keys;\
sh ${CLIENT_INSTALL_DIR}/initialise_auth_keys.sh;\
"
        fi

        # Test again
        if [ "$(ssh_test)" = "no" ]; then
            echo "It looks like we cannot access ${CLIENT_USER_IP} without a password."
            echo "Please make sure that the host is UP."
            echo ""
            echo "If so, then ensure that the contents of:"
            echo "             cat ${rsa_pub}"
            echo "to the remote file:"
            echo "             ${CLIENT_USER_IP}:/mnt/us/usbnet/etc/authorized_keys"
            echo ""
            exit 255
        else
            echo "Configured ${CLIENT_USER_IP} for passwordless access" 
        fi
    fi
}

copy_over_scripts_and_assets() {
    local tmpdir=$(mktemp -d)    
    cp -rv ${REPO_DIR}/client/installables/* ${tmpdir}
    
    if [ "$SERVER_IP" = "" ]; then
        echo "Could not find SERVER_IP. Quitting." >&2
        exit 255
    fi
    
    sed -i "s|REPLACE_ON_INSTALL_BY_SCRIPT|$SERVER_IP|" ${tmpdir}/run_sleep.sh
    cd ${tmpdir}
    SCP_CMD "-r ." "${CLIENT_USER_IP}:${CLIENT_INSTALL_DIR}"
    cd -;

    # Copy over needed binaries and libs (e.g. tmux)
    echo "Copying over binaries and libs"
    local USBNET_INSTALL_DIR=/mnt/us/usbnet/
    cd ${REPO_DIR}/client/bin/;
    SCP_CMD "-r ." "${CLIENT_USER_IP}:${USBNET_INSTALL_DIR}/bin/"
    cd ${REPO_DIR}/client/lib/;
    SCP_CMD "-r ." "${CLIENT_USER_IP}:${USBNET_INSTALL_DIR}/lib/"
}

run_iptables() {
    SSH_CMD "sh ${CLIENT_INSTALL_DIR}/iptables.sh"
}


attempt_to_ssh &&  ## passwordless ssh login, made install dir
    copy_over_scripts_and_assets &&
    ##run_iptables &&
    echo "[Finit] ${CLIENT_USER_IP} is now configured. You can now ssh in without a password, and Amazon services are kept out."