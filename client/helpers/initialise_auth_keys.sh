#!/bin/bash

# Tell kindle where ssh is to look for authorized keys.
USBNET_DIR="/mnt/base-us/usbnet/"
AUTHOR_KEY="${USBNET_DIR}/etc/authorized_keys"
SSHD_CONFG="${USBNET_DIR}/etc/config"
# - Authorized Keys Initialise
echo "[*] Configuring SSHD file to point to Authorized Keys"
mkdir -p ${USBNET_DIR} 2>/dev/null
sed -i -r "s|^\#?AuthorizedKeysFile.*|AuthorizedKeysFile ${AUTHOR_KEY}|" ${USBNET_DIR}/etc/sshd_config &&
    touch ${AUTHOR_KEY} &&
    echo "[*] Configuring USBNET file to enable SSHD on boot"
## Configure SSH to always start on reboot

restart_usbnet_initctl(){
    /sbin/initctl restart usbnetd &&
        /sbin/initctl restart usbnet &&
        sleep 5 &&
        /sbin/initctl restart usbnet   ## need to restart twice for some reason
}
restart_usbnet_etc(){
    /etc/init.d/usbnetd stop && /etc/init.d/usbnetd start &&
        /etc/init.d/usbnetd stop && /etc/init.d/usbnetd start &&
        sleep 5 &&
        /etc/init.d/usbnetd stop && /etc/init.d/usbnetd start
    ## need to restart twice for some reason
}
restart_usbnet(){
    initctl 2>/dev/null;
    if [ $? = 127 ]; then
        restart_usbnet_etc
    else
        restart_usbnet_initctl
    fi
}

sed -i 's|^USE_WIFI=.*|USE_WIFI="true"|' ${SSHD_CONFG} &&
    sed -i 's|^USE_OPENSSH.*|USE_OPENSSH="true"|' ${SSHD_CONFG} &&
    sed -i 's|^QUIET_DROPBEAR.*|QUIET_DROPBEAR="false"|' ${SSHD_CONFG} &&
    sed -i 's|^TWEAK_MAC_ADDRESS.*|TWEAK_MAC_ADDRESS="false"|' ${SSHD_CONFG} &&
    touch ${USBNET_DIR}/auto &&
    echo "" > ${USBNET_DIR}/DISABLED_AUTO &&  ## newline needed?
    restart_usbnet

## BELOW THIS IS A COMMENT CONTAINING THE RSA PUB KEY FOR THE SERVER
## AND IS GENERATED AUTOMATICALLY FROM THE INSTALL SCRIPT.